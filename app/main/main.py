from flask import Flask, render_template,json, redirect, url_for, request,session
from flask_sqlalchemy import SQLAlchemy
from flask import flash
from flask_login import LoginManager

from flask_login import UserMixin

from flask_login import login_user




app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:root@localhost:3306/hotel'

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = 'dev'

login_manager = LoginManager(app)  # 实例化扩展类
db = SQLAlchemy(app)
# 配置数据库
class User(db.Model):
    __tablename__ = "User"
    id = db.Column(db.Integer, primary_key=True)
    userName = db.Column(db.String(50), nullable=False)
    password = db.Column(db.String(100), nullable=False)
    phone = db.Column(db.String(20), nullable=False)
    email = db.Column(db.String(20), nullable=False)
    room = db.Column(db.String(50))

    def to_dict(self):
        dic = {
            "id": self.id,
            "userName": self.customerName,
            "password": self.password,
            "phone": self.phone,
            "address": self.address,
            "room": self.roomHobby,
        }
        return dic
#添加订单
class room_typle(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    orderName = db.Column(db.String(50))
    ophone =db.Column(db.String(50))
    roomtyple = db.Column(db.String(50))
    litime = db.Column(db.String(50))

    def to_dict(self):
        dic = {
            "id": self.id,
            "orderName": self.orderName,
            "ophone": self.ophone,
            "roomtyple": self.roomtyple,
            "litime": self.litime,

        }
        return dic


# 主页的访问路径
@app.route('/')
def index_views1():
    return render_template("index.html", params=locals())

# 登录页面的访问路径
@app.route('/reg', methods=['GET', 'POST'])
def login_views():
    if request.method == "GET":
        # 判断cookies中是否有username
        if 'uname' in request.cookies:
            username = request.cookies['uname']
            # 判断uname的值是否为admin
            user = User.query.filter_by(username=username).first()
            if user:
                return redirect('/')
        return render_template("login.html")
    else:
        # 接受前端传递过来的数据
        username = request.form.get('username')
        # print(type(username), username)
        password = request.form.get('password')
        # 使用接受的用户名和密码到数据库中查询
        user = User.query.filter_by(username=username, password=password).first()
        # 如果用户存在，将信息保存进session并重定向回首页，否则重定向登陆页
        if user:
            resp = redirect('/')
            # resp = make_response("保存cookies成功")
            resp.set_cookie("uname", username, 60 * 60 * 24 * 365)
            resp.set_cookie("upwd", password)
            session['id'] = user.id
            session['uname'] = user.username
            return resp
        else:
            errMsg = "用户名或密码不正确"
            return render_template('login.html', errMsg=errMsg)


# 注册页面的访问路径

@app.route('/reg', methods=['GET', 'POST'])
def register_views():
    if request.method == "GET":
        return render_template("login.html")
    else:
        # 获取文本框的值并赋值给user实体对象
        user = User()
        user.username = request.form['uname']
        user.password = request.form['upwd']
        user.email = request.form['uemail']
        user.hone = request.form['uphone']
        # 将数据保存进数据库   --注册
        db.session.add(user)
        # 手动提交，目的是为了获取提交后的user的id
        db.session.commit()
        # 当user成功插入数据库之后，程序会自动将所有信息取出来再赋值给user
        # 完成登录的行为操作
        session['uid'] = user.id
        session['uname'] = user.username
        return redirect('/')

#添加订单
@app.route("/add")
def addroom():
    room = room_typle
    room.id = request.args['id']
    room.orderName = request.args['ordername']
    room.roomtyple = request.args['roomtyple']
    room.litime = request.args['litime']
    return json.dumps(request.args['id'])
@app.route('/myOrder')
def operation_manager_views1():
    return render_template("myOrder.html")







if __name__ == '__main__':
    app.run(debug=True)